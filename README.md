#Vagrant Widget for Übersicht
Are you like me, and spin up several Vagrant Boxes, only to forget which are actually running.. Well this is a Vagrant widget for the awesome [Übersicht](http://tracesof.net/uebersicht/ "Übersicht"), that'll give you a quick status of boxes on your system. 

#Installation
Clone into your Übersicht widgets directory... thats it.

#Screenshot
![Screenshot](https://bytebucket.org/smyther42/vagrantstatus.widget/raw/16d4c5d1fef2a405c8f11d39d4ece0dc92c0a4c6/screenshot.png "Yup, a screenshot")

#Todo
- Some type of sorting so boxes of the same state are together
- Your ideas here

Feel free to clone this and hack the hell out of it!