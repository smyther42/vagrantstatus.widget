#!/bin/bash

# This function will start our JSON text.
startJSON() {
  echo '{'
  echo '  "boxes" : ['
}

# This function will return a single block of JSON for a single service.
exportService() {
  echo '  {'
  echo '    "id":"'${id}'",'
  echo '    "name":"'${name}'",'
  echo '    "provider":"'${provider}'",'
  echo '    "state":"'${state}'",'
  echo '    "directory":"'${directory}'"'
  echo '  },'

}

# This function will finish our JSON text.
endJSON() {
  echo '  ]'
  echo '}'
}

export LANG="C"
export LC_ALL="en_US.UTF-8"

i=0

startJSON
#id       name    provider   state   directory
while read line #col1 col2 col3 col4 col5 col6 col7
do 

[[ -z $line ]] && break

if [ $i -gt 1 ]
then

while IFS=' ' read id name provider state directory
do
	if [ -z $id ]
		then
      echo '  {}'
			endJSON
			exit
		else
			exportService
		fi
done

fi

i+=1

done < <(vagrant global-status)

