# CSS Style

abortColour = 'rgba(167, 73, 80, 1)'
abortOpacity = '0.2'

savedColour = 'rgba(85, 165, 199, 1)'
savedOpacity = '0.6'

runningColour = 'rgba(85, 165, 199, 1)'
runningOpacity = '0.9'

titleColour = 'rgba(50,50,50,0.9)'

textColour = '#ffffff'

style: """
  margin:0
  padding:0
  left:19px
  top: 340px
  width: 372px
  color: #{textColour}
  cellspacing: 0
  padding: 10px
  text-shadow: 0 0 1px rgba(#000, 0.5)
  font-weight: normal
  font-size: 15px

  .row.title
    background: #{titleColour}
    font-weight: bold
    font-size: 20px
    padding: 10px

  .row
    font-weight: 200
    font-family: Helvetica Neue
    margin-bottom: 5px
    border-radius: 4px
    padding: 15px 10px 5px 10px

  .row>div
    box-sizing: border-box
    display: inline-block

  .dir 
    width: 100%
    position: relative

  .dir > span
    float: right
    text-transform: capitalize
    font-weight: bold

  .small
    position: absolute
    top: -12px
    left: 0
    right: 0
    font-size: 8px
    font-weight: 200
    text-align: right

  .small > span
    margin-left: 20px

  .small > span.left
    float: left
    margin-left: 0

  div.aborted
    background: #{abortColour}
    opacity: #{abortOpacity}

  div.saved
    background: #{savedColour}
    opacity: #{savedOpacity}

  div.running
    background: #{runningColour}
    opacity: #{runningOpacity}
"""

# Execute the shell command.
command: "vagrantstatus.widget/getStatus.sh"

# Set the refresh frequency (milliseconds).
refreshFrequency: 10000

# Render the output.
render: (output) -> """
<div id='vagrant-container'></div>
"""

# Update the rendered output.
update: (output, domEl) -> 

  data = JSON.parse output
  html = ""

  html += "<div class='row title'><div>Vagrant Boxes</div></div>"

  for box in data.boxes

    if box.id?

      dirArray = box.directory.split('/')
      dir = dirArray[dirArray.length-1]
      html += "<div class='"+box.state+" row'>"
      html += "<div class='dir'><div class='small'><span class='left'>Project</span><span>ID: "+box.id+"</span><span>Name: "+box.name+"</span><span>Provider: "+box.provider+"</span></div>"+dir+"<span>"+box.state+"</span></div>"
      html += "</div>"

  $('#vagrant-container').html(html)
 

